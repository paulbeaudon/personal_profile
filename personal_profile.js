var navbar = document.getElementById("navbar");
var links = navbar.getElementsByClassName("link");

//var menuOpen = false;

/*
orginally used to highlight active menu when clicked - now using scrolling position to highlight menu (below window listener)
let activate = (linkNumber)=>{
  let current = document.getElementsByClassName("active");
  current[0].className = current[0].className.replace(" active", "");

  links[linkNumber].className += " active";

  if(!screenWatch.matches){
    moveMenu();
  }
};*/

var main = document.getElementById("main");
var sections = main.getElementsByClassName("sections");

window.addEventListener("scroll", function (event) {
  var scroll = this.scrollY;
  for(let x=0; x<sections.length; x++){
    if(scroll >= sections[x].getBoundingClientRect().top + scroll-400){
      let current = document.getElementsByClassName("active");
      current[0].className = current[0].className.replace(" active", "");
      links[x].className += " active";
    }
  }
});

//function moveMenu(){
  //if(!screenWatch.matches){ // matches media of screen width > 1000px
  //  if(!menuOpen){
//      document.getElementById('navbar').style.height = "100%";
//      menuOpen = true;
//    }else{
//      document.getElementById('navbar').style.height = "4em";
//      menuOpen = false;
//    }
 // }
//}
function myFunction(screenWatch){
    if (screenWatch.matches) { // matches media of screen width < 1000px
        document.getElementById('navbar').style.height = "100%";
    }else{
      document.getElementById('navbar').style.height = "4em";
      menuOpen = false;
    }
}

var screenWatch = window.matchMedia("(min-width: 1000px)");
myFunction(screenWatch);
screenWatch.addListener(myFunction);

let elementCircles = document.getElementsByClassName('circle');
let elementSvgs = document.getElementsByClassName('circle-svg');

for(let x=0; x<elementCircles.length; x++){
  elementCircles[x].addEventListener('mouseover', function(e){
    elementSvgs[x].style.filter = 'brightness(0) invert(100%)';
  })
  elementCircles[x].addEventListener('mouseout', function(e){
    elementSvgs[x].style.filter = '';
  })
}
/*
function resize(){
  document.getElementById('screenwidth').innerHTML = window.innerWidth;
  document.getElementById('screenheight').innerHTML = window.innerHeight;
}
window.onresize = resize;
*/
